const data = {
  products: [
    {
      //_id: '1',
      name: 'Dollay Pickle',
      slug: 'dollay pickle',
      category: 'Pickle',
      image: '/images/p1.jpg',
      prices: 200,
      countInStock: 10,
      brand: 'Chutney',
      rating: 4.5,
      numReviews: 10,
      descriptions: 'Vegetable Mixed',
    },
    {
      //_id: '2',
      name: 'Adidas Shoe',
      slug: 'adidas shoe',
      category: 'Shoe',
      image: '/images/p2.jpg',
      prices: 1500,
      countInStock: 1,
      brand: 'Adidas',
      rating: 4.5,
      numReviews: 6,
      descriptions: 'High Quality',
    },
    {
      //_id: '3',
      name: 'Nike Slim Pant',
      slug: 'Nike Slim Pant',
      category: 'Pant',
      image: '/images/p3.jpg',
      prices: 2000,
      countInStock: 4,
      brand: 'Nike',
      rating: 4.5,
      numReviews: 10,
      descriptions: 'Flexible',
    },
    {
      //_id: '4',
      name: 'Zara T-Shirt',
      slug: 'zara t-shirt',
      category: 'T-Shirt',
      image: '/images/p4.jpg',
      prices: 1000,
      countInStock: 3,
      brand: 'Zara',
      rating: 4.5,
      numReviews: 10,
      descriptions: 'Cotton',
    },
    {
      //_id: '4',
      name: 'Zara T-Shirt',
      slug: 'zara t-shirt',
      category: 'T-Shirt',
      image: '/images/p4.jpg',
      prices: 1000,
      countInStock: 3,
      brand: 'Zara',
      rating: 4.5,
      numReviews: 10,
      descriptions: 'Cotton',
    },
  ],
};
export default data;
